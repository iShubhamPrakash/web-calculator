# web-calculator
A simple web calculator. One of my early projects during my web development course.</br>
Click the link below to have a look at it:</br>
<a href="https://i-shubhamprakash.github.io/web-calculator/" target="_blank"> https://i-shubhamprakash.github.io/web-calculator/ </a>
</br>
Technologies used- </br>
HTML5 </br>
CSS3 </br>
JavaScript </br>
jQuery </br>


